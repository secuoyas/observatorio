// here you can add a custom storybook theme
// https://storybook.js.org/docs/react/configure/theming#create-a-theme-quickstart

import { create } from "@storybook/theming/create"

export default create({
  base: "light",

  brandTitle: "Observatorio",
  brandImage: "observatorio-icon.png",
})
