import React from "react"
import { configure } from "@storybook/react"
import { ThemeProvider } from "styled-components"
import { theme } from "../src/utils/themeGet"
import { parseTheme } from "@sqymagma/theme"

import _mainTheme from "../src/themes/theme"

global.___loader = {
  enqueue: () => {},
  hovering: () => {},
}

global.__PATH_PREFIX__ = ""

window.___navigate = pathname => {
  action("NavigateTo:")(pathname)
}

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
}
export const decorators = [
  story => <ThemeProvider theme={parseTheme(_mainTheme)}>{story()}</ThemeProvider>,
]

// configure(load, module)
configure(require.context("../src/stories", true, /[\w\d\s]+\.story\.(js|mdx)$/), module)
