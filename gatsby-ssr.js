import React, { Fragment } from "react"
import { ThemeProvider } from "styled-components"
import { Reset } from "styled-reset"
import { Normalize } from "styled-normalize"
import { parseTheme } from "@sqymagma/theme"
import { GlobalStyles } from "./src/themes/Global"
import mainTheme from "./src/themes/theme"

const defaultTheme = parseTheme({ ...mainTheme })

export const wrapPageElement = ({ element }) => {
  return (
    <ThemeProvider theme={defaultTheme}>
      <Fragment>
        <Reset />
        <Normalize />
        <GlobalStyles />
        {element}
      </Fragment>
    </ThemeProvider>
  )
}

export const onRenderBody = ({
  setHeadComponents,
  setPostBodyComponents,
  setPreBodyComponents,
}) => {
  setHeadComponents([
    <script
      key="gtm"
      dangerouslySetInnerHTML={{
        __html: `<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TFN4TZ');</script>
<!-- End Google Tag Manager -->`,
      }}
    />,
  ])

  setPreBodyComponents([
    <script
      key="gtm-no-script"
      dangerouslySetInnerHTML={{
        __html: `<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TFN4TZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->`,
      }}
    />,
  ])

  setPostBodyComponents([
    <script
      key="linkedin_id"
      dangerouslySetInnerHTML={{
        __html: `
      _linkedin_partner_id = "2469770"; window._linkedin_data_partner_ids =
      window._linkedin_data_partner_ids || [];
      window._linkedin_data_partner_ids.push(_linkedin_partner_id);
      `,
      }}
    />,
    <script
      key="linkedin_id_2"
      dangerouslySetInnerHTML={{
        __html: `
        (function(){
          var s = document.getElementsByTagName("script")[0];
          var b = document.createElement("script");
          b.type = "text/javascript";b.async = true;
          b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
          s.parentNode.insertBefore(b, s);
        })();
    `,
      }}
    />,
  ])
}
