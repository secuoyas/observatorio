const fs = require("fs")
const camelCase = require("camelcase")
const path = require("path")

// Devuelve un array con TODOS los archivos/carpetas de un path
function walk(dir) {
  var results = []
  var list = fs.readdirSync(dir)
  list.forEach(function(file) {
    file = dir + "/" + file
    var stat = fs.statSync(file)
    if (stat && stat.isDirectory()) {
      /* Recurse into a subdirectory */
      results = results.concat(walk(file))
    } else {
      /* Is a file */
      results.push(file)
    }
  })
  return results
}

const isSvgFile = file => path.extname(file) === ".svg"

const allSVGS = walk(`./src/svg-icons`).filter(isSvgFile)
const allSVGSRelativePath = allSVGS.map(file => path.relative("./src/svg-icons", file))
const svgBaseName = allSVGS.map(file => path.basename(file, path.extname(file)))
const svgCamelCase = svgBaseName.map(file => camelCase(file))
const svgPascalCase = svgBaseName.map(file => camelCase(file, { pascalCase: true }))

// INDEX FILE
const indexFile = `
import React from "react"
import styled, { css } from "styled-components"
import { space, color, layout, system } from "styled-system"
import { Box } from "@sqymagma/elements"

${svgPascalCase.map((f, idx) => `import ${f} from "./svg/${allSVGSRelativePath[idx]}"`).join("\n")}

const filterProps = ["width", "height", "fill", "color", "bg", "display"]

const Base = css\`
  *\{$\{system({
    fill: {
      property: "fill",
      scale: "color",
    },
  })}}
\`

${svgPascalCase
  .map(
    f => `const Base${f} = styled(Box)
  .attrs(p => ({ as: ${f} }))
  .withConfig({shouldForwardProp: (prop) => ![...filterProps].includes(prop)})
\`\$\{Base\}\``
  )
  .join("\n")}

const Icons = ({ name, props}) => \{
  const list = \{
    ${svgPascalCase.map((f, idx) => `${svgCamelCase[idx]} : <Base${f} {...props} />,`).join("\n")}
    }
    return list[name]
\}

const IcnSystem = ({ name, ...props }) => {
  return Icons({ name, props })
}

${svgPascalCase.map((f, idx) => `export { Base${f} as ${f} }`).join("\n")}

export const iconList = [
  ${svgPascalCase.map(i => `"${i}"`)}
]

export default IcnSystem
 `

fs.writeFileSync("./src/IconSystem/index.js", indexFile)
fs.writeFileSync("./src/IconSystem/listIcons.json", JSON.stringify(svgCamelCase))
