exports.createPages = async ({ actions, graphql, reporter }) => {
  const result = await graphql(`
    query {
      allMdx(
        filter: { fileAbsolutePath: { regex: "/tendencias/" } }
        sort: { fields: frontmatter___number }
      ) {
        nodes {
          frontmatter {
            slug
            description: socialgraphDescription
          }
        }
      }
    }
  `)

  if (result.errors) {
    reporter.panic("failed to create trends pages", result.errors)
  }

  const trends = result.data.allMdx.nodes

  trends.forEach(trend => {
    actions.createPage({
      path: `/trends/${trend.frontmatter.slug}`,
      component: require.resolve("./src/components/TrendDetail.js"),
      context: {
        slug: trend.frontmatter.slug,
        description: trend.frontmatter.description,
      },
    })
  })
}
