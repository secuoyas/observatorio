require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

const theme = require("./src/themes/theme")
let breakpoints = {}
theme.mediaquery.mediaqueries.forEach(mq => {
  breakpoints[mq.label] = mq.minWidth ? `(min-width: ${mq.minWidth})` : `(min-width: 0)`
})

module.exports = {
  pathPrefix: ``,
  siteMetadata: {
    title: `Observatorio`,
    titleTemplate: "%s | Observatorio",
    description: `Un espacio de debate, divulgación, diálogo y encuentro sobre el futuro inmediato de la Educación Superior`,
    author: "Secuoyas",
    twitterUsername: `@secuoyas`,
    url: "https://observatorio.secuoyas.com",
    image: "socialgraph-site.png",
    imageAlt: "Observatorio es un espacio para la investigación y la reflexión creado por Secuoyas",
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-styled-components`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: "gatsby-plugin-web-font-loader",
      options: {
        google: {
          families: ["Work Sans:400,500,600,700:latin", "Source Serif Pro:400,600,700:latin"],
        },
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Observatorio`,
        short_name: `Observatorio`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#0D2B4F`,
        display: `minimal-ui`,
        icon: `static/observatorio-favicon.svg`, // this is at least 512*512, all device icons are generated automatically from this
      },
    },
    {
      resolve: "gatsby-plugin-react-svg",
      options: {
        rule: {
          include: `${__dirname}/src/IconSystem/svg/`,
        },
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `tendencias`,
        path: `${__dirname}/data/tendencias/`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `current-events`,
        path: `${__dirname}/data/current-events/`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/images/`,
      },
    },
    {
      resolve: "gatsby-plugin-mdx",
      options: {
        defaultLayouts: {
          default: require.resolve("./src/components/Layout.js"),
        },
      },
    },
    {
      resolve: "gatsby-plugin-breakpoints",
      options: {
        queries: breakpoints,
      },
    },
    {
      resolve: "gatsby-plugin-mailchimp",
      options: {
        endpoint: process.env.MAILCHIMP_ENDPOINT,
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: process.env.GA_TRACKING_ID,
      },
    },
    {
      resolve: `gatsby-plugin-linkedin-insight`,
      options: {
        partnerId: `2469770`,

        // Include LinkedIn Insight in development.
        // Defaults to false meaning LinkedIn Insight will only be loaded in production.
        includeInDevelopment: false,
      },
    },
  ],
}
