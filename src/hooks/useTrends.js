import { graphql, useStaticQuery } from "gatsby"

const useTrends = () => {
  const data = useStaticQuery(trendsQuery)
  const trends = data.trends.nodes
    .map(trend => ({
      ...trend.frontmatter,
      image: trend.frontmatter.image.sharp.fluid,
      body: trend.body,
    }))
    .sort((a, b) => a.number - b.number)

  return trends
}

export default useTrends

export const trendsQuery = graphql`
  query {
    trends: allMdx(filter: { fileAbsolutePath: { regex: "/tendencias/" } }) {
      nodes {
        frontmatter {
          title
          lead
          slug
          number
          imageAlt
          image {
            sharp: childImageSharp {
              fluid(maxWidth: 400, quality: 90) {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
          }
        }
      }
    }
  }
`
