import { useContext } from "react"
import { ThemeContext } from "styled-components"

function useStyledTheme() {
  const theme = useContext(ThemeContext)
  return theme || {}
}

export default useStyledTheme
