import { graphql, useStaticQuery } from "gatsby"

const useCurrentEvents = () => {
  const data = useStaticQuery(trendsQuery)

  return data.currentEvents.nodes.map(item => ({
    ...item.frontmatter,
  }))
}

export default useCurrentEvents

export const trendsQuery = graphql`
  query {
    currentEvents: allMdx(filter: { fileAbsolutePath: { regex: "/current-events/" } }) {
      nodes {
        frontmatter {
          content
          label
          url
        }
      }
    }
  }
`
