import React from "react"
import Button from "../components/showcase/Button"

export default {
  title: "Example/Button",
  component: Button,
  argTypes: {
    bg: { control: "color" },
    color: { control: "color" },
  },
}

const Template = args => <Button {...args} />

export const Primary = Template.bind({})
Primary.args = {
  color: "textOnInteractive01",
  bg: "interactive01",
  children: "Button",
}
