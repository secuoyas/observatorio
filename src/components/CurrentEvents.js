import React from "react"
import { Box, Columns, Text } from "@sqymagma/elements"
import useCurrentEvents from "../hooks/useCurrentEvents"
import CurrentItem from "./CurrentItem"
import { TopLineContainer } from "../styles"

const CurrentEvents = props => {
  const currentEvents = useCurrentEvents()
  return (
    <Box {...props} as="section" mb="xl">
      <TopLineContainer>
        <Text as="h3" textStyle="smallBold" mb={{ default: "m", m: "xl" }}>
          Sigue la conversación
        </Text>
        <Columns hs={{ default: "l", xl: "l" }} flexWrap="wrap" vs={{ default: "xl", m: "0" }}>
          {currentEvents.map(item => (
            <CurrentItem
              content={item.content}
              label={item.label}
              url={item.url}
              width={{ default: "100%", m: 1 / 4 }}
              key={item.url}
              mb={{ xl: "3xl", xxl: "4xl" }}
            />
          ))}
        </Columns>
      </TopLineContainer>
    </Box>
  )
}

export default CurrentEvents
