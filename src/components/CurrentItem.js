import React from "react"
import { Flex, Text } from "@sqymagma/elements"
import ReadMoreLink from "./ReadMoreLink"

const CurrentItem = ({ content, label, url, ...props }) => (
  <Flex height={{ default: "auto", m: "140px" }} flexDirection="column" {...props}>
    <Text as="p" textStyle="bodyAlt" mb={{ default: "s", m: 0 }}>
      {content}
    </Text>
    <ReadMoreLink label={label} href={`${url}`} external={"true"} mt="auto" />
  </Flex>
)
export default CurrentItem
