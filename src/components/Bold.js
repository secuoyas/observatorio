import React from "react"
import { Text } from "@sqymagma/elements"

const Bold = ({ children, textStyle, color }) => (
  <Text textStyle={textStyle || "bodyBold"} color={color || "text01"}>
    {children}
  </Text>
)
export default Bold
