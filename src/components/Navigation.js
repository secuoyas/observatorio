import { Box, Flex } from "@sqymagma/elements"
import { Link as GatsbyLink } from "gatsby"
import { Text } from "@sqymagma/elements"
import React from "react"
import styled from "styled-components"
import SocialNetworks from "./SocialNetworks"
import { navigation } from "../../data"

const Navigation = ({ inverseHeader, ...props }) => {
  return (
    <Box role="navigation" aria-label="main" {...props}>
      <Flex as="ul">
        {navigation.map((link, idx) => (
          <Item
            key={idx}
            inverseHeader={inverseHeader}
            label={link.label}
            to={link.internal && link.url}
            href={link.external && link.url}
            as={link.internal ? GatsbyLink : "a"}
          />
        ))}
        <SocialNetworks />
      </Flex>
    </Box>
  )
}

const StyledLink = styled(Text)`
  text-decoration: none;
  cursor: pointer;
  position: relative;
`
const Item = ({ label, last, inverseHeader, tag, newsletterLink, ...props }) => {
  return (
    <Box as="li" mr={!last && "l"}>
      <StyledLink
        textStyle="smallBold"
        color={inverseHeader ? "textOnInverse01" : "text01"}
        {...props}
      >
        {label}
      </StyledLink>
    </Box>
  )
}

export default Navigation
