![Logo](./static/observatorio-logo.svg){: width="200px"}

# Observatorio

Sitio para Observatorio (http://observatorio.secuoyas.com), construido sobre el Gatsby Magma Starter.

**Scripts**:

- `npm run start` Lanza el proyecto en dev mode
- `npm run start:device` Lanza el proyecto en dev mode en la red local
- `npm run build` Compila el proyecto en la carpeta /public
- `npm run serve` Para visualizar el proyecto compilado
- `npm run clean` Limpia la cache de Gatsby
- `npm run reset` Borra directorios /.cache /public y /node_modules
- `npm run format` Formatea con prettier
- `npm run build:icons` Optimiza los svgs de la carpeta /svg-icons y los disponibiliza como componentes
- `npm run build` Lanza Storybook
- `npm run build-sb` Compila Storybook

**Ramas y deployment**:

- master
- release/production: para empujar el proyecto a producción
- cms: es una prueba de concepto (desactualizada con respecto a las otras dos) para coger los contenidos de strapi. Existe un repositorio llamado [observatorio-api](https://bitbucket.org/secuoyas/observatorio-api/src) con el que funciona

_Actualmente **no existe un entorno de testing** asociado a alguna rama tipo release/test release/develop o release/staging _

**El repositorio trabaja con**:

### Magma

Con `@sqymagma/theme`y `@sqymagma/elements`.
La información sbre el uso de temas y su aplicacióna los componentes de Magma puede encontrarse en [la documentación de Notion](https://www.notion.so/magmadesignsystem/3edfb46c3ca24ce9bc23f457a05f21b2?v=4b719eb3963c4f56a16de1db1b26f48d).

### Storybook 6

Storybook 6 está instalado y configurado, pero **aún no se ha creado ninguna historia para documentar los componentes de este proyecto**. Las historias pueden escribirse como mdx o js. Para lanzar Storybook, usamos el cmd `npm run sb``

### Sistema de iconos

Para hacerlos disponibles como componentes hay que correr el comando `npm run build:icons` o `yarn build:icons`
Los archivos `svgs` se mantendrán intactos en la carpeta original y se hará una copia optimizada en el directorio final.
Más información sobre el uso de los iconos, en la documentación de Magma

### Features de Gatsby

- Hot reloading, con Gatsby
- [Code splitting](https://www.gatsbyjs.com/docs/how-code-splitting-works/#reach-skip-nav)
- [Optimización de imágenes](https://www.gatsbyjs.com/docs/working-with-images/#optimizing-images-with-gatsby-image)
- [Data queries con GraphQL](https://www.gatsbyjs.org/tutorial/part-five/#introducing-graphiql)

### Plugins de Gatsby

El proyecto trabaja sobre varios plugins de Gatsby, a destacar:

#### helmet

Para el trabajo de SEO. Existe un componente Seo.js que renderiza los metatags necesarios para cada página. Open graph y Twitter graph están configurados.

#### gatsby-source-filesystem y gatsby-plugin-mdx

Para crear nodos en la capa de graphql de los contenidos que existen en la carpeta /data

#### gatsby-plugin-breakpoints

Para los estilos responsive. La configuración está en gatsby-config.js, se suministran al plugin los breakpoints del theme. Al contrario que el uso de un hook (e.g. useMediaQueries de react-beautiful-hooks) no da problemas en el build

#### gatsby-plugin-mailchimp

Hay una cuenta de mailchimp asociada a este proyecto. Cuando un nuevo usuario se subscribe a través de cualquiera de formularios, ingresa en la lista de Mailchimp mediante este plugin

**Responsable**: maria.simo@secuoyas.com
